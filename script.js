new AirDatepicker('#datapicker');

datapicker.onclick = function(){
    document.getElementById('placeholder-datapicker').style.transition = '1s';
    document.getElementById('placeholder-datapicker').style.fontSize = ' 1.3vh';
    document.getElementById('placeholder-datapicker').style.marginTop = '3px';
    document.getElementById('placeholder-datapicker').style.marginLeft = '10px';
};

linedate.onclick = function(){
    document.getElementById('placeholder-linedate').style.transition = '1s';
    document.getElementById('placeholder-linedate').style.fontSize = ' 1.3vh';
    document.getElementById('placeholder-linedate').style.marginTop = '3px';
    document.getElementById('placeholder-linedate').style.marginLeft = '10px';
};
summ.onclick = function(){
    document.getElementById('placeholder-summ').style.transition = '1s';
    document.getElementById('placeholder-summ').style.fontSize = ' 1.3vh';
    document.getElementById('placeholder-summ').style.marginTop = '3px';
    document.getElementById('placeholder-summ').style.marginLeft = '10px';
};
replenishment_summ.onclick = function(){
    document.getElementById('placeholder-replenishment').style.transition = '1s';
    document.getElementById('placeholder-replenishment').style.fontSize = ' 1.3vh';
    document.getElementById('placeholder-replenishment').style.marginTop = '3px';
    document.getElementById('placeholder-replenishment').style.marginLeft = '10px';
};
checkin.onclick = function(){
    var check = document.getElementById('check');
    if(check.checked)
        document.getElementById('input-checking').style.display = 'none';
    else
        document.getElementById('input-checking').style.display = 'block';
};

calcul.onclick = function(){
var select  = document.getElementById('select').value;
var datapicker = $("#datapicker").val();
var linedate = $("#linedate").val();
var summ = $("#summ").val();
var persent = $("#persent").val();
var replenishment_summ = $("#replenishment_summ").val();
if(select == 'Month'){
    $("#MyForm").validate({
        rules: {
            linedate:{
                required: true,
                min: 1,
                max: 60
            },
            datapicker:{
                required: true,
                date: true,
            },
            summ:{
                required: true,
                number: true,
                min: 1000,
                max: 3000000
            },
            persent:{
                required: true,
                number: true,
                min: 3,
                max: 100
            },
            replenishment_summ:{
                required: true,
                number: true,
                min: 0,
                max: 3000000
            }
        }
    });
    var data = {
        "datapicker": datapicker, 
        "linedate": linedate, 
        "summ": summ, 
        "percent": persent, 
        "replenishment_summ": replenishment_summ
    };
    
    var jsonData = JSON.stringify(data);
    $.ajax({
        method: "POST",
        url: "calc.php",
        data: {data: jsonData},
        success: function(data){
            var rez = $.parseJSON(data);
            console.log(rez);
            for(var k in rez)
                $('#info').html("₽" + Math.trunc(rez[k]));
        }
        });
    
}
else if(select == 'Year'){
    $("#MyForm").validate({
        rules: {
            linedate:{
                required: true,
                min: 1,
                max: 5
            },
            datapicker:{
                required: true,
                date: true,
            },
            summ:{
                required: true,
                number: true,
                min: 1000,
                max: 3000000
            },
            persent:{
                required: true,
                number: true,
                min: 3,
                max: 100
            },
            replenishment_summ:{
                required: true,
                number: true,
                min: 0,
                max: 3000000
            }
        }
    });
    var data = {
        "datapicker": datapicker, 
        "linedate": linedate * 12, 
        "summ": summ, 
        "percent": persent, 
        "replenishment_summ": replenishment_summ
    };
    
    var jsonData = JSON.stringify(data);
    $.ajax({
        method: "POST",
        url: "calc.php",
        data: {data: jsonData},
        success: function(data){
            var rez = $.parseJSON(data);
            console.log(rez);
            for(var k in rez)
                $('#info').html("₽" + Math.trunc(rez[k]));
        }
        });
}

// $.ajax({
//     method: "POST",
//     url: "calc.php",
//     data: {datapicker: datapicker, linedate: linedate, summ: summ, persent:persent, replenishment_summ: replenishment_summ},
//     success: function(data){
//         var rezult = $.parseJSON(data);
//         console.log(rezult);
//     }
//     });
};

