<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="assets/node_modules/air-datepicker/air-datepicker.css">
    <title>Document</title>
</head>

<body>
    <header class="header">
        <div class="header-content">
            <img src="assets/images/logo-light 1.png" alt="logo">
            <p>Deposit Calculator</p>
        </div>
    </header>
    <section class="section" id="sect">
        <div class="content">
            <div class="content-head">
                <h1 class="title">Депозитный калькулятор</h1>
                <p class="description">Калькулятор депозитов позволяет рассчитать ваши доходы после внесения суммы на счет в банке по опредленному тарифу.</p>
            </div>
            <form action="" id="MyForm">
            <div class="cont-full">
                <div class="date">
                    <label for="" class="label-date">
                        <input type="text" id="datapicker" class="datapickervk" value="" name="datapicker">
                        <span class="placeholder-datapicker" id="placeholder-datapicker">Дата открытия</span>
                    </label>
                    <div class="content-timeline">
                        <label for="" class="input-timeline">
                            <input type="text" class="linedate" id="linedate" name="linedate">
                            <span class="placeholder-datapicker pl-dt" id="placeholder-linedate">Срок вклада</span>
                        </label>

                        <select name="" id="select" class="timeline">
                            <option value="Month">Месяц</option>
                            <option value="Year">Год</option>
                        </select>
                    </div>
                </div>
                <div class="summ-persent">
                    <label for="" class="input-timeline summ">
                        <input type="text" class="input-summ" id="summ" name="summ">
                        <span class="placeholder-datapicker" id="placeholder-summ">Сумма вклада</span>
                    </label>
                    <label for="" class="input-timeline">
                        <input type="text" class="linedate  summvk" value="10.00" id="persent" name="persent">
                        <span class="placeholder-persent" id="placeholder-persent">Процентная ставка, % годовых</span>
                    </label>
                </div>
                <div class="replenishment">
                    <div class="checking">
                        <div>
                            <input type="checkbox" name="" id="check" class="checkbox">
                            <label for="check" id="checkin"></label>
                        </div>
                        <p>Ежемесячное пополнение вклада</p>
                    </div>
                    <div class="input-checking" id="input-checking">
                        <label for="" class="input-timeline" >
                            <input type="text" class="input-summ" id="replenishment_summ" name="replenishment_summ">
                            <span class="placeholder-datapicker" id="placeholder-replenishment">Сумма пополнения вклада</span>
                        </label>
                    </div>
                </div>
                <div class="calulate">
                    <input type="button" value="Рассчитать" class="button" id="calcul">
                </div>
            </div>
            <h4>Cумма к выплате</h4>
            <h1 id="info">

            </h1>
            </form>
        </div>

    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="assets/node_modules/air-datepicker/air-datepicker.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js"></script>
    <script src="script.js"></script>
</body>

</html>